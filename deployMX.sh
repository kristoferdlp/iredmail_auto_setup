#! /bin/bash

# check arguments
if [ $# -eq 0 ]
  then echo "Usage: bash iRedMail_script.sh [ip domainName password]*"
  exit
fi

# ssh to server and do installation
slow() {
  echo "Creating a separate thread for root@$1"
  # copy authorized_keys to server
  scp -o StrictHostKeyChecking=no authorized_keys "root@$1:~/.ssh/"
  scp -o StrictHostKeyChecking=no installMx.sh "root@$1:~/"
  ssh -o StrictHostKeyChecking=no "root@$1" "bash installMx.sh $1 $2 $3" &
}

# call separate thread for each ip address
while [ $# -ge 3 ]
do
  slow $1 $2 $3 &
  shift
  shift
  shift
done

echo "Program ran."
