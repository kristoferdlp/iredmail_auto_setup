export DOMAIN_ADMIN_PASSWD_PLAIN="$3"
export DOMAIN_ADMIN_PASSWD="$3"
export SITE_ADMIN_PASSWD="$3"
export FIRST_USER="postmaster"
export FIRST_USER_PASSWD="$3"
export FIRST_USER_PASSWD_PLAIN="$3"
export FIRST_DOMAIN="mail.$2"
export DOMAIN_ADMIN_NAME="postmaster"
export SITE_ADMIN_NAME="postmaster@mail.$2"

wget "https://bitbucket.org/zhb/iredmail/downloads/iRedMail-0.9.4.tar.bz2"
tar xjf iRedMail-0.9.4.tar.bz2
cd iRedMail-0.9.4
wget "https://bitbucket.org/kristoferdlp/iredmail_auto_setup/downloads/config"
IREDMAIL_DEBUG='NO' AUTO_USE_EXISTING_CONFIG_FILE=y AUTO_INSTALL_WITHOUT_CONFIRM=y AUTO_CLEANUP_REMOVE_SENDMAIL=y AUTO_CLEANUP_REMOVE_MOD_PYTHON=y AUTO_CLEANUP_REPLACE_FIREWALL_RULES=y AUTO_CLEANUP_RESTART_IPTABLES=y AUTO_CLEANUP_REPLACE_MYSQL_CONFIG=y AUTO_CLEANUP_RESTART_POSTFIX=n bash iRedMail.sh
postconf -e content_filter=
postconf -e smtpd_sender_restrictions="reject_unknown_sender_domain reject_non_fqdn_sender reject_unlisted_sender permit_mynetworks permit_sasl_authenticated check_sender_access pcre:/etc/postfix/sender_access.pcre"
cd ~
touch done
reboot
